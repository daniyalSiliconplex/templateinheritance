from django.shortcuts import render


def renderPage(request):

    products=[{'title':'Product 1!','desc':'04-02-1962 - Product 1!'},
           {'title': 'Product 2!', 'desc': '04-02-1963 - Product 2!'},
           {'title': 'Product 3!', 'desc': '04-02-1964 - Product 3!'},
           {'title': 'Product 4!', 'desc': '04-02-1964 - Product 4!'},
           {'title': 'Product 5!', 'desc': '04-02-1964 - Product 5!'},
           {'title': 'Product 6!', 'desc': '04-02-1964 - Product 6!'}]

    return render(request,'Pages/postsPage.html',{'products':products,'pageTitle':'Products'})
